"""Add custom_data column to Protocol model

Revision ID: 1aa828d34af6
Revises: 8ee2b6d6cfe0
Create Date: 2022-03-29 19:38:10.678598

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '1aa828d34af6'
down_revision = '8ee2b6d6cfe0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('protocol', sa.Column('custom_data', postgresql.JSON(astext_type=sa.Text()), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('protocol', 'custom_data')
    # ### end Alembic commands ###
