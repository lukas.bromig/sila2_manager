from .crud_service import service
from .crud_item import item
from .crud_user import user
from .crud_job import job
from .crud_scheduled_job import scheduled_job
from .crud_workflow import workflow
from .crud_protocol import protocol
from .crud_database import database

# For a new basic set of CRUD operations you could just do

# from .base import CRUDBase
# from app.models.item import Item
# from app.schemas.item import ItemCreate, ItemUpdate

# item = CRUDBase[Item, ItemCreate, ItemUpdate](Item)
