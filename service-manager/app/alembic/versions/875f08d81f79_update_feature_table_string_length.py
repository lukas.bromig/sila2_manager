"""Update Feature table string length

Revision ID: 875f08d81f79
Revises: e7fb92370e5c
Create Date: 2022-03-21 16:08:43.040164

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '875f08d81f79'
down_revision = 'e7fb92370e5c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
