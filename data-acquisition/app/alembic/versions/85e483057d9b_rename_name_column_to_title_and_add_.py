"""Rename 'name' column to 'title' and add 'description' column to Database model

Revision ID: 85e483057d9b
Revises: 306f1f7290fc
Create Date: 2022-01-16 17:31:10.199803

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '85e483057d9b'
down_revision = '306f1f7290fc'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('database', sa.Column('title', sa.String(), nullable=True))
    op.add_column('database', sa.Column('description', sa.String(), nullable=True))
    op.drop_index('ix_database_name', table_name='database')
    op.create_index(op.f('ix_database_title'), 'database', ['title'], unique=False)
    op.drop_column('database', 'name')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('database', sa.Column('name', sa.VARCHAR(), autoincrement=False, nullable=True))
    op.drop_index(op.f('ix_database_title'), table_name='database')
    op.create_index('ix_database_name', 'database', ['name'], unique=False)
    op.drop_column('database', 'description')
    op.drop_column('database', 'title')
    # ### end Alembic commands ###
